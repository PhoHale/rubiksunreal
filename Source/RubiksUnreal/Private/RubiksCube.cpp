// Fill out your copyright notice in the Description page of Project Settings.

#include "RubiksUnreal.h"
#include "RubiksCube.h"
#include "RubiksPiece.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "iTween/iTween.h"

// Sets default values
ARubiksCube::ARubiksCube()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	this->ScrambleCounter = 10;
	this->StepsCounter = 0;
	this->bGameStarted = false;

	//Set default cube size
	this->CubeSize = 3;

	//Allow the player to start the first movement
	this->PlayerMovementEnabled = true;

	this->ClickStartPiece = nullptr;
    
    this->bIsCameraMoving = false;

	//Creates the piece rotator
	DummyRoot = CreateDefaultSubobject <USceneComponent>(FName("Dummy Root"));
	SetRootComponent(DummyRoot);

	PieceRotator = CreateDefaultSubobject<USceneComponent>(FName("Piece Rotator"));
	PieceRotator->AttachTo(GetRootComponent());

    SpringArm = CreateDefaultSubobject<USpringArmComponent>(FName("Spring Arm"));
    SpringArm->AttachTo(GetRootComponent());
    
    SpringArm->bDoCollisionTest = false;
    SpringArm->bInheritPitch= false;
    SpringArm->bInheritRoll = false;
    SpringArm->bInheritYaw = false;
    
    //Update camera's arm length for the size
    float zoomStart = CAMERA_ZOOM_START_NORMAL;
    FString platform = UGameplayStatics::GetPlatformName();
    if(platform.Equals(FString(TEXT("IOS"))) || platform.Equals(FString(TEXT("Android")))) {
        zoomStart= CAMERA_ZOOM_START_MOBILE;
    }
    
    SpringArm->TargetArmLength = zoomStart + ((this->CubeSize-2)*CAMERA_ZOOM_STEP);
    
    CubeCamera = CreateDefaultSubobject<UCameraComponent>(FName("Cube Camera"));
    CubeCamera->AttachTo(SpringArm);
	
}

// Called when the game starts or when spawned
void ARubiksCube::BeginPlay()
{
	Super::BeginPlay();
	this->BuildCube(this->CubeSize);
}

// Called every frame
void ARubiksCube::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (this->bIsScrambling) {
		return;
	}

	//Camera movement logic
	if (this->bIsCameraMoving) {
		if (SpringArm->GetComponentRotation().Euler().Y + this->CameraMovementAxis.Y * 2 > CAMERA_Y_ANGLE_LIMIT) {
			SpringArm->SetRelativeRotation(FRotator::MakeFromEuler(FVector(0, CAMERA_Y_ANGLE_LIMIT, SpringArm->GetComponentRotation().Euler().Z)));
		}
		else if (SpringArm->GetComponentRotation().Euler().Y + this->CameraMovementAxis.Y * 2 < -CAMERA_Y_ANGLE_LIMIT) {
			SpringArm->SetRelativeRotation(FRotator::MakeFromEuler(FVector(0, -CAMERA_Y_ANGLE_LIMIT, SpringArm->GetComponentRotation().Euler().Z)));
		}
		else {
			SpringArm->AddWorldRotation(FRotator(0, this->CameraMovementAxis.X * 2, 0));
			SpringArm->AddRelativeRotation(FRotator(this->CameraMovementAxis.Y * 2, 0, 0));
		}
	}
	else {
		this->CameraMovementAxis = FVector::ZeroVector;
	}

	//Get player controller
	APlayerController * pController = UGameplayStatics::GetPlayerController(GetWorld(), 0);

	//Used for touch controls
	bool bTouchPressed = false;
	FVector touchPosition;
	pController->GetInputTouchState(ETouchIndex::Touch1, touchPosition.X, touchPosition.Y, bTouchPressed);

	

	//Reset mechanics
	if (!pController->IsInputKeyDown(EKeys::LeftMouseButton) && !bTouchPressed) {

		this->PlayerMovementEnabled = true;
		this->bIsCameraMoving = false;
		this->ClickStartPiece = nullptr;
		this->ClickStartPosition = FVector::ZeroVector;
		this->ClickStartNormal = FVector::ZeroVector;

		
        
	}
	else if (this->PlayerMovementEnabled == true && !this->bIsCubeAnimating){ //Start movement detection
		FVector mouseWorldPosition;
		FVector mouseWorldDirection;
		if (bTouchPressed) {
			//Project touch position from screen to 3d world
			pController->DeprojectScreenPositionToWorld(touchPosition.X, touchPosition.Y, mouseWorldPosition, mouseWorldDirection);
		}
		else {
			//Project mouse position from screen to 3d world
			pController->DeprojectMousePositionToWorld(mouseWorldPosition, mouseWorldDirection);
		}
		//Calculating the end of the ray
		FVector traceEnd = mouseWorldPosition + (mouseWorldDirection * 2000);

		FHitResult outHit;
		FCollisionQueryParams TraceParams(FName(TEXT("Rubiks Piece Trace")), false);
		TraceParams.bTraceComplex = false;

		//Trace a ray to find a rubiks piece
		if (GetWorld()->LineTraceSingle(outHit, mouseWorldPosition, traceEnd, ECC_Visibility, TraceParams) && !this->bIsCameraMoving && !this->IsCubeSolved()) {
			//Verifies if the object is a rubiks piece
			if (outHit.GetActor()->Tags.Contains(CUBE_PIECE_TAG)) {
				if (this->ClickStartPiece == nullptr) {
					this->ClickStartPiece = (ARubiksPiece *)outHit.GetActor();
					this->ClickStartPosition = outHit.ImpactPoint;
					this->ClickStartNormal = outHit.ImpactNormal;
					UE_LOG(LogActor, Warning, TEXT("Rubiks Piece Found: %s"), *(this->ClickStartPiece->GetName()));
				}
				else { //Already dragging the mouse over a piece
					FVector direction = outHit.ImpactPoint - this->ClickStartPosition;
					int32 dragDistance = direction.Size();
					if (dragDistance > DRAG_DISTANCE) { //Detect movement
						FVector normalizedDirection = direction.GetSafeNormal();
						UE_LOG(LogActor, Warning, TEXT("Normal: %f, %f, %f -Direction: %f, %f, %f"), this->ClickStartNormal.X, this->ClickStartNormal.Y, this->ClickStartNormal.Z, normalizedDirection.X, normalizedDirection.Y, normalizedDirection.Z);
						this->PlayerMovementEnabled = false;
						//Start the rotation process
						this->StepsCounter++;
						this->RotateFromPiece(this->ClickStartNormal, normalizedDirection, this->ClickStartPiece);
					}
				}
			}			
        } else if (this->ClickStartPiece == nullptr){
            this->bIsCameraMoving = true;
			//Update camera movement axis for camera rotation
			if (bTouchPressed) {
				this->CameraMovementAxis.X = touchPosition.X - LastTouchPosition.X;
				this->CameraMovementAxis.Y = LastTouchPosition.Y - touchPosition.Y;
				this->CameraMovementAxis /= 5.0f;

				if (this->CameraMovementAxis.Size() > DRAG_DISTANCE) {
					LastTouchPosition = touchPosition;
					this->CameraMovementAxis = FVector::ZeroVector;
				}
			} else {
				pController->GetInputMouseDelta(this->CameraMovementAxis.X, this->CameraMovementAxis.Y);
			}
        }
	}

	//Updating touch drag movement
	if (bTouchPressed)
	{
		LastTouchPosition = touchPosition;
	}
}

void ARubiksCube::DestroyCube()
{
	PiecesToRotate.Empty();
	for (int32 x = 0; x < Pieces.Num(); x++) {
		Pieces[x]->Destroy();
	}

	Pieces.Empty();
	PieceRotator->SetRelativeRotation(FRotator(0, 0, 0));
	SpringArm->SetRelativeLocation(FVector(0, 0, 0));
}

void ARubiksCube::BuildCube(int32 size)
{
	//Prevent to build a new cube if it is scrambling
	if (this->bIsScrambling) {
		return;
	}

	//Set new cube size
	this->bGameStarted = false;
	this->StepsCounter = 0;
	this->CubeSize = size;
	UWorld * gWorld = GetWorld();

    //Update camera's arm length for the new size
    float zoomStart = CAMERA_ZOOM_START_NORMAL;
    
    FString platform = UGameplayStatics::GetPlatformName();
    if(platform.Equals(FString(TEXT("IOS"))) || platform.Equals(FString(TEXT("Android")))) {
        zoomStart= CAMERA_ZOOM_START_MOBILE;
    }

	SpringArm->TargetArmLength = zoomStart + ((this->CubeSize-2)*CAMERA_ZOOM_STEP);

	//Set PieceRotator and camera's arm to the center of the new cube
	float centerOffset = (CUBE_EXTENT * (this->CubeSize - 1)) / 2;
	PieceRotator->SetRelativeLocation(FVector(centerOffset, centerOffset, centerOffset));
	SpringArm->SetRelativeLocation(FVector(centerOffset, centerOffset, centerOffset));
	SpringArm->SetRelativeRotation(FRotator::MakeFromEuler(FVector(0, -30, 0)));

	UE_LOG(LogTemp, Warning, TEXT("Cube Creation!"));
	//Create cube based on its size
	for (int i = 0; i < this->CubeSize; i++) {
		for (int j = 0; j < this->CubeSize; j++) {
			for (int k = 0; k < this->CubeSize; k++) {
				//Spawn only pieces that belongs to a wall
				if (gWorld && (i == 0 || i == this->CubeSize-1 || j == 0 || j == this->CubeSize-1 || k ==0 || k == this->CubeSize-1)) { 
					//Spawn a rubiks piece
					FActorSpawnParameters params;
					params.Owner = this;
					ARubiksPiece * piece = gWorld->SpawnActor<ARubiksPiece>(PieceClass, FVector(CUBE_EXTENT * j, CUBE_EXTENT * i, CUBE_EXTENT * k), FRotator(0.0f, 0.0f, 0.0f), params);
					piece->AttachRootComponentToActor(this, NAME_None, EAttachLocation::KeepWorldPosition, false);
					piece->Tags.Add(CUBE_PIECE_TAG);
					Pieces.Add(piece);
				}
			}
		}
	}
}

void ARubiksCube::Scramble()
{
	//Choose a random group based on a axis
	int random = FMath::RandRange(0, 2);
	ERotationGroup::RotationGroup rotationGroupAxis = ERotationGroup::X;;
	FRotator angle = FRotator(0, 0, 90);
	switch (random)
	{
	case 0:
		rotationGroupAxis = ERotationGroup::X;
		angle = FRotator(0, 0, 90);
		break;
	case 1:
		rotationGroupAxis = ERotationGroup::Y;
		angle = FRotator(90, 0, 0);
		break;
	case 2:
		rotationGroupAxis = ERotationGroup::Z;
		angle = FRotator(0, 90, 0);
		break;
	default:
		break;
	}

	//Choose a random piece for the group
	random = FMath::RandRange(0, Pieces.Num() - 1);
	ARubiksPiece * randomPiece = Pieces[random];

	//Choose a random direction
	random = FMath::RandRange(0, 1);
	if (random) {
		angle *= -1;
	}
	
	//Scramble!
	RotateGroup(FName("Scramble"), randomPiece, rotationGroupAxis, angle, .1f);
}

void ARubiksCube::Scramble(int32 steps)
{
	//Not scramble if it is already scrambling
	if (this->bIsScrambling) {
		return;
	}

	//Start scramble chain
	this->ScrambleCounter = steps;
	this->bIsScrambling = true;
	this->StepsCounter = 0;
	this->bGameStarted = false;
	this->Scramble();
}

int32 ARubiksCube::GetSteps()
{
	return this->StepsCounter;
}

bool ARubiksCube::IsCubeSolved()
{
	if (!this->bGameStarted) {
		return false;
	}
	bool solved = true;
	for (int x = 0; x < Pieces.Num(); x++) {
		if (!Pieces[x]->IsAtStartPosition()) {
			solved = false;
			break;
		}
	}

	return solved;
}


void ARubiksCube::RotateFromPiece(FVector normal, FVector direction, class ARubiksPiece * piece)
{ 
    UE_LOG(LogActor, Warning, TEXT("Rotate Group!"));
	if (normal.Equals(FVector(0,0,1))) { //Top Face
		UE_LOG(LogActor, Warning, TEXT("Top face!"));
		if (direction.X > 0.5f) { 
			RotateGroup(FName("Group Rotation"), this->ClickStartPiece, ERotationGroup::RotationGroup::Y, FRotator(-90, 0, 0));
		}
		else if (direction.X < -0.5f) { 
			RotateGroup(FName("Group Rotation"), this->ClickStartPiece, ERotationGroup::RotationGroup::Y, FRotator(90, 0, 0));
		}
		else if (direction.Y > 0.5f) { 
			RotateGroup(FName("Group Rotation"), this->ClickStartPiece, ERotationGroup::RotationGroup::X, FRotator(0, 0, 90));
		}
		else if (direction.Y < -0.5f) {
			RotateGroup(FName("Group Rotation"), this->ClickStartPiece, ERotationGroup::RotationGroup::X, FRotator(0, 0, -90));
		}
	}
	else if (normal.Equals(FVector(0, 0, -1))) { //Bottom Face
		UE_LOG(LogActor, Warning, TEXT("Bottom face!"));
        if (direction.X > 0.5f) {
			RotateGroup(FName("Group Rotation"), this->ClickStartPiece, ERotationGroup::RotationGroup::Y, FRotator(90, 0, 0));
        }
        else if (direction.X < -0.5f) { 
			RotateGroup(FName("Group Rotation"), this->ClickStartPiece, ERotationGroup::RotationGroup::Y, FRotator(-90, 0, 0));
        }
        else if (direction.Y > 0.5f) {
			RotateGroup(FName("Group Rotation"), this->ClickStartPiece, ERotationGroup::RotationGroup::X, FRotator(0, 0, -90));
        }
        else if (direction.Y < -0.5f) { 
			RotateGroup(FName("Group Rotation"), this->ClickStartPiece, ERotationGroup::RotationGroup::X, FRotator(0, 0, 90));
		}
	}
	else if (normal.Equals(FVector(1, 0, 0))) { //Back face
        UE_LOG(LogActor, Warning, TEXT("Back face!"));
        if (direction.Z > 0.5f) { 
			RotateGroup(FName("Group Rotation"), this->ClickStartPiece, ERotationGroup::RotationGroup::Y, FRotator(90, 0, 0));
        }
        else if (direction.Z < -0.5f) { 
			RotateGroup(FName("Group Rotation"), this->ClickStartPiece, ERotationGroup::RotationGroup::Y, FRotator(-90, 0, 0));
        }
        else if (direction.Y > 0.5f) { 
			RotateGroup(FName("Group Rotation"), this->ClickStartPiece, ERotationGroup::RotationGroup::Z, FRotator(0, 90, 0));
        }
        else if (direction.Y < -0.5f) { 
			RotateGroup(FName("Group Rotation"), this->ClickStartPiece, ERotationGroup::RotationGroup::Z, FRotator(0, -90, 0));
        }
	}
	else if (normal.Equals(FVector(-1, 0, 0))) { //Front Face
		UE_LOG(LogActor, Warning, TEXT("Front face!"));
        if (direction.Z > 0.5f) { 
			RotateGroup(FName("Group Rotation"), this->ClickStartPiece, ERotationGroup::RotationGroup::Y, FRotator(-90, 0, 0));
        }
        else if (direction.Z < -0.5f) { 
			RotateGroup(FName("Group Rotation"), this->ClickStartPiece, ERotationGroup::RotationGroup::Y, FRotator(90, 0, 0));
        }
        else if (direction.Y > 0.5f) { 
			RotateGroup(FName("Group Rotation"), this->ClickStartPiece, ERotationGroup::RotationGroup::Z, FRotator(0, -90, 0));
        }
        else if (direction.Y < -0.5f) {
			RotateGroup(FName("Group Rotation"), this->ClickStartPiece, ERotationGroup::RotationGroup::Z, FRotator(0, 90, 0));
        }
	}
	else if (normal.Equals(FVector(0, -1, 0))) { //Right Face
		UE_LOG(LogActor, Warning, TEXT("Right face!"));
        if (direction.Z > 0.5f) {
			RotateGroup(FName("Group Rotation"), this->ClickStartPiece, ERotationGroup::RotationGroup::X, FRotator(0, 0, 90));
        }
        else if (direction.Z < -0.5f) { 
			RotateGroup(FName("Group Rotation"), this->ClickStartPiece, ERotationGroup::RotationGroup::X, FRotator(0, 0, -90));
        }
        else if (direction.X > 0.5f) { 
			RotateGroup(FName("Group Rotation"), this->ClickStartPiece, ERotationGroup::RotationGroup::Z, FRotator(0, 90, 0));
        }
        else if (direction.X < -0.5f) {
			RotateGroup(FName("Group Rotation"), this->ClickStartPiece, ERotationGroup::RotationGroup::Z, FRotator(0, -90, 0));
        }
	}
	else if (normal.Equals(FVector(0, 1, 0))) { //Left Face
		UE_LOG(LogActor, Warning, TEXT("Left face!"));
        if (direction.Z > 0.5f) {
			RotateGroup(FName("Group Rotation"), this->ClickStartPiece, ERotationGroup::RotationGroup::X, FRotator(0, 0, -90));
        }
        else if (direction.Z < -0.5f) {
			RotateGroup(FName("Group Rotation"), this->ClickStartPiece, ERotationGroup::RotationGroup::X, FRotator(0, 0, 90));
        }
        else if (direction.X > 0.5f) { 
			RotateGroup(FName("Group Rotation"), this->ClickStartPiece, ERotationGroup::RotationGroup::Z, FRotator(0, -90, 0));
        }
        else if (direction.X < -0.5f) { 
			RotateGroup(FName("Group Rotation"), this->ClickStartPiece, ERotationGroup::RotationGroup::Z, FRotator(0, 90, 0));
        }
	}
    
}

void ARubiksCube::RotateGroup(FName tweenName, class ARubiksPiece * piece, ERotationGroup::RotationGroup groupAxis, FRotator rotation, float speed)
{
	//Clean array of the pieces that will rotate
	PiecesToRotate.Empty();

	//Remove parent from all pieces
	for (int32 x = 0; x < Pieces.Num(); x++) {
		Pieces[x]->DetachRootComponentFromParent(true);
	}

	//Reset rotation from PieceRotator
	PieceRotator->SetRelativeRotation(FRotator(0, 0, 0));
	//Add all pieces from the same group as the given piece to the PiecesToRotate array (used fabs just to prevent possible minimal erros)
	if (groupAxis == ERotationGroup::RotationGroup::X) {
		for (int32 x = 0; x < Pieces.Num(); x++) {
			if (fabs(Pieces[x]->GetActorLocation().X - piece->GetActorLocation().X) < 0.2) {
				PiecesToRotate.Add(Pieces[x]);
			}
		}
	}
	else if (groupAxis == ERotationGroup::RotationGroup::Y) {
		for (int32 x = 0; x < Pieces.Num(); x++) {
			if (fabs(Pieces[x]->GetActorLocation().Y - piece->GetActorLocation().Y) < 0.2) {
				PiecesToRotate.Add(Pieces[x]);
			}
		}
	}
	else if (groupAxis == ERotationGroup::RotationGroup::Z) {
		for (int32 x = 0; x < Pieces.Num(); x++) {
			if (fabs(Pieces[x]->GetActorLocation().Z - piece->GetActorLocation().Z) < 0.2) {
				PiecesToRotate.Add(Pieces[x]);
			}
		}
	}

	//Set all the pieces to rotate as child of the PieceRotator
	for (int32 x = 0; x < PiecesToRotate.Num(); x++) {
		PiecesToRotate[x]->AttachRootComponentTo(PieceRotator, NAME_None, EAttachLocation::KeepWorldPosition);
	}

	//Rotate PieceRotator
	UiTween::ComponentRotateFromToSimple(tweenName, PieceRotator, FRotator(0, 0, 0), rotation, ECoordinateSpace::self, true, speed, EEaseType::linear, "", this, nullptr, this);
}

void ARubiksCube::OnTweenComplete_Implementation(AiTweenEvent* eventOperator, AActor* actorTweening, USceneComponent* componentTweening, UWidget* widgetTweening, FName tweenName, FHitResult sweepHitResultForMoveEvents, bool successfulTransform)
{
	if (tweenName == FName("Group Rotation")) {
		UE_LOG(LogActor, Warning, TEXT("Finished Rotation"));
		this->bIsCubeAnimating = false;
		this->PlayerMovementEnabled = true;
	}
	else if (tweenName == FName("Scramble")) {
		UE_LOG(LogActor, Warning, TEXT("Scrambling"));
		ScrambleCounter--;
		if (ScrambleCounter >= 0) {
			this->Scramble();
		}
		else {
			this->bGameStarted = true;
			this->bIsScrambling = false;
			this->bIsCubeAnimating = false;
			this->PlayerMovementEnabled = true;
		}
	}
}

void ARubiksCube::OnTweenStart_Implementation(AiTweenEvent* eventOperator, AActor* actorTweening, USceneComponent* componentTweening, UWidget* widgetTweening, FName tweenName)
{
    UE_LOG(LogActor, Warning, TEXT("Started Rotation!"));
	this->bIsCubeAnimating = true;
	this->ClickStartPiece = nullptr;
	this->ClickStartNormal = FVector::ZeroVector;
	this->ClickStartPosition = FVector::ZeroVector;
}

