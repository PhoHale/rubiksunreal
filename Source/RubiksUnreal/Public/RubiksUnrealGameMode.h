// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "RubiksUnrealGameMode.generated.h"

/** GameMode class to specify pawn and playercontroller */
UCLASS(minimalapi)
class ARubiksUnrealGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ARubiksUnrealGameMode(const FObjectInitializer& ObjectInitializer);
};



